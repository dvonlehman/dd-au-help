Getting Started
======================

* Open your web browser *(Example: Google Chrome)*.
* Navigate to `http://manager.dockdogsevents.com <http://manager.dockdogsevents.com>`_.
* Enter your **ADMIN** username and password in the corresponding boxes.
* Click **LOGIN**.

.. tip:: You must use a computer (laptop/desktop) to interface with this website. Using a mobile phone or tablet is not supported at this time.

**************************************************
Assisting A Handler With Locating Their Username
**************************************************

* Once you've logged in via the admin login you're able to search for Handler accounts.
* Click **MANAGE** --> **HANDLERS & TEAMS** from the menu along the top of the page.
* Enter the Handler's **first** & **last name** into the search boxes on the left hand side of the page.
* Click **SEARCH**

.. tip:: Handlers can also locate their **username and/or password** by visiting `http://dockdogsevents.com <http://dockdogsevents.com>`_ and clicking on **FORGOT USERNAME** or **FORGOT PASSWORD**. *Their details will be sent via email via an automated system, if they do not receive the email please ask them to check their spam/junk filter. If they still cannot locate the email confirm that the email address on file is correct and that's the email they are checking.*

***************************************************
Assisting A Handler With Resetting Their Password
***************************************************

* Handlers can reset their password via the self-service automated method at `https://dockdogsevents.com <https://dockdogsevents.com>`_ as long as they already know their **USERNAME** and **EMAIL ADDRESS** in our database.
* From your admin account you can **MANAGE** --> **HANDLERS & TEAMS** from the menu along the top of the page and locate a Handler via the search filters to assist a Handler with the password reset process.
* Once you've located the Handler whose password you need to reset click the **ACTION** icon/button on the far right hand side of the corresponding row and click **RESET PASSWORD**.

.. tip:: Handler passwords are encrypted and subsequently can only be reset, viewing the current password isn't possible. This is to protect the security of the Handler's account.

***************************************************
Assisting A Handler With Adding a Team (Dog)
***************************************************

* Often times a Handler may get stuck when attempting to register due to not having added a team (dog) to their account.
* You can confirm that a Handler does/does not have teams on their account by clicking on the **ACTION** button on the **MANAGE** --> **HANDLERS & TEAMS** screen of a given handler and selecting **EDIT TEAM**.

.. image:: images/handler-has-teams.gif

* If a Handler doesn't have any teams they must add a team (dog) to their account prior to being able to register for any events.
* A Handler can add a Team (dog) to their account by logging in at `https://dockdogsevents.com <https://dockdogsevents.com>`_ with their username and password, selecting **TEAMS** from the upper right hand corner, clicking on **ADD NEW TEAM** to enter their dogs details, then **SAVE** the record.
